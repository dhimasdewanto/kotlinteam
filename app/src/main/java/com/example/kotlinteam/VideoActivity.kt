package com.example.kotlinteam

import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.MediaController
import kotlinx.android.synthetic.main.activity_video.*

class VideoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)

        val videoPath = "android.resource://$packageName/${R.raw.videoview}"
//        val videoPath = "https://download.blender.org/peach/bigbuckbunny_movies/BigBuckBunny_320x180.mp4"
//        val videoPath = "http://distribution.bbb3d.renderfarming.net/video/mp4/bbb_sunflower_1080p_30fps_normal.mp4" // ERROR
//        val videoPath = "http://distribution.bbb3d.renderfarming.net/video/mp4/bbb_sunflower_1080p_60fps_normal.mp4" // ERROR

        val uri = Uri.parse(videoPath)
        videoViewMain.setVideoURI(uri)

        val mediaController = MediaController(this)
        videoViewMain.setMediaController(mediaController)
        mediaController.setAnchorView(videoViewMain)

        videoViewMain.requestFocus()
    }

    override fun onStart() {
        super.onStart()
        videoViewMain.start()
    }
}
