package com.example.kotlinteam

import android.app.DatePickerDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setEditTextName()
        setEditTextDob()
        setButtonInsertClick()
        setButtonDeviceVideo()
        setButtonStreamingVideo()
    }

    private fun setButtonDeviceVideo() {
        buttonDeviceVideo.setOnClickListener {
            val intent = Intent(this, VideoActivity::class.java)
            startActivity(intent)
        }
    }

    private fun setButtonStreamingVideo() {
        buttonStreamingVideo.setOnClickListener {
            val intent = Intent(this, VideoStreamingActivity::class.java)
            startActivity(intent)
        }
    }

    private fun setEditTextName() {
        editTextName.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                //
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                //
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (editTextName.text.toString().length < 6) {
                    editTextName.setError("Minimum characters length is 6")
                }
            }
        })
    }

    private fun setButtonInsertClick() {
        buttonInsert.setOnClickListener {
            Log.i("Data", "Name: ${editTextName.text.toString()}")
            Log.i("Data", "Date of Birth: ${editTextDob.text.toString()}")
            Log.i("Data", "Gender: ${getGender()}")
        }
    }

    private fun setEditTextDob() {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        editTextDob.setOnClickListener {
            val datePickerDialog = DatePickerDialog(this, DatePickerDialog.OnDateSetListener{view, year, month, dayOfMonth ->
                editTextDob.setText("$dayOfMonth/$month/$year")
            }, year, month, day)

            datePickerDialog.show()
        }
    }

    private fun getGender(): String {
        if (radioButtonMale.isChecked) {
            return "MALE"
        }
        if (radioButtonFemale.isChecked) {
            return "FEMALE"
        }
        return ""
    }
}
